package cr.ac.ucenfotec.proyecto.bl;

import java.util.ArrayList;

public class Provincia {
    private String nombre;
    private int identificador;
    static ArrayList<String>provincias = new ArrayList<>();

    // CONSTRUCTORES

    public Provincia(){
        setNombre("sin nombre");
        setIdentificador(0);
    }
    public Provincia(String nombre, int identificador){
        setNombre(nombre);
        setIdentificador(identificador);
    }

    // SETTERS & GETTERS


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public static ArrayList<String> getProvincias() {
        return provincias;
    }

    public static void setProvincias(ArrayList<String> provincias) {
        Provincia.provincias = provincias;
    }

    // TO STRING

    @Override
    public String toString() {
        return "Provincia{" +
                "nombre='" + nombre + '\'' +
                ", identificador=" + identificador +
                '}';
    }
}
