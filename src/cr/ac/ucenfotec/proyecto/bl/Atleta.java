package cr.ac.ucenfotec.proyecto.bl;

public class Atleta extends Usuario {
    private String avatar;
    private String fechaNacimiento;
    private int edad;
    private char genero;
    private String direccion;

    // CONSTRUCTORES

    public Atleta(){
        super();
        setAvatar("sin avatar");
        setFechaNacimiento("sin fecha de nacimiento");
        setEdad(0);
        setGenero('-');
        setDireccion("sin direccion");
    }

    public Atleta(String avatar, String fechaNacimiento, int edad, char genero, String direccion, String nombre,
                  String apellido, String usuario, String clave, String identificacion, Pais nombrePais){

        super(nombre, apellido, usuario, clave,identificacion, nombrePais);
        setAvatar(avatar);
        setFechaNacimiento(fechaNacimiento);
        setEdad(edad);
        setGenero(genero);
        setDireccion(direccion);
    }


    // SETTERS & GETTERS


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    // TO STRING


    @Override
    public String toString() {
        return "Atleta{" +
                "avatar='" + avatar + '\'' +
                ", fechaNacimiento='" + fechaNacimiento + '\'' +
                ", edad=" + edad +
                ", genero=" + genero +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
