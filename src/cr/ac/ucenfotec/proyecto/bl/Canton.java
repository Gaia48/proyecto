package cr.ac.ucenfotec.proyecto.bl;

public class Canton {
    private String nombre;
    private int identificador;

    // CONSTRUCTORES

    public Canton(){
        setNombre("sin nombre");
        setIdentificador(0);
    }

    public Canton(String nombre, int identificador){
        setNombre(nombre);
        setIdentificador(identificador);
    }
    // SETTERS & GETTERS


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    // TO STRING


    @Override
    public String toString() {
        return "Canton{" +
                "nombre='" + nombre + '\'' +
                ", identificador=" + identificador +
                '}';
    }
}
