package cr.ac.ucenfotec.proyecto.bl;

public class Usuario {
    private String nombre;
    private String apellido;
    private String usuario;
    private String clave;
    private String identificacion;
    private Pais nombrePais;

    // CONSTRUCTORES

    public Usuario(){
        setNombre("sin nombre");
        setApellido("sin apellido");
        setUsuario("sin usuario");
        setClave("sin clave");
        setIdentificacion("sin identificacion");
        setNombrePais(null);

    }
    public Usuario(String nombre, String apellido, String usuario, String clave, String identificacion,
                   Pais nombrePais){
        setNombre(nombre);
        setApellido(apellido);
        setUsuario(usuario);
        setClave(clave);
        setIdentificacion(identificacion);
        setNombrePais(null);
    }


    // SETTERS & GETTERS


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Pais getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(Pais nombrePais) {
        this.nombrePais = nombrePais;
    }

    // TO STRING


    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", usuario='" + usuario + '\'' +
                ", clave='" + clave + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", nombrePais=" + nombrePais +
                '}';
    }
}
