package cr.ac.ucenfotec.proyecto.bl;

public class Distrito {
    private String nombre;
    private int identificador;

    // CONSTRUCTOR

    public Distrito(){
        setNombre("sin nombre");
        setIdentificador(0);
    }
    public Distrito(String nombre, int identificador){
        setNombre(nombre);
        setIdentificador(identificador);
    }
    // GETTERS & SETTERS


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    // To STRING


    @Override
    public String toString() {
        return "Distrito{" +
                "nombre='" + nombre + '\'' +
                ", identificador=" + identificador +
                '}';
    }
}
