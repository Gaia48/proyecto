package cr.ac.ucenfotec.proyecto.bl;

public class Admin extends Usuario{

    private boolean registrado;

    // CONSTRUCTORES

    public Admin(){
        super();
        setRegistrado(false);
    }
    public Admin(boolean registrado, String nombre, String apellido, String usuario, String clave,
                 String identificacion, Pais nombrePais){

        super(nombre, apellido, usuario,clave, identificacion,nombrePais);
        setRegistrado(registrado);

    }


    // SETTERS & GETTERS


    public boolean isRegistrado() {
        return registrado;
    }

    public void setRegistrado(boolean registrado) {
        this.registrado = registrado;
    }
}
