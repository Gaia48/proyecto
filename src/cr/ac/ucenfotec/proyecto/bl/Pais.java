package cr.ac.ucenfotec.proyecto.bl;

import java.util.ArrayList;

public class Pais {
    private String nombrePais;
    private int codigoGenerado;
    static ArrayList<String>paises = new ArrayList<>();

    // COSTRUCTORES


    public Pais (){

        setNombre("Sin nombre");
        setCodigoGenerado(0);
    }

    public Pais(String nombrePais, int codigoGenerado){
        setNombre(nombrePais);
        setCodigoGenerado(codigoGenerado);
    }




    // SETTERS & GETTERS


    public String getNombre() {
        return nombrePais;
    }

    public void setNombre(String nombre) {
        this.nombrePais = nombre;
    }

    public int getCodigoGenerado() {
        return codigoGenerado;
    }

    public void setCodigoGenerado(int codigoGenerado) {
        this.codigoGenerado = codigoGenerado;
    }

    public static ArrayList<String> getPaises() {
        return paises;
    }

    public static void setPaises(ArrayList<String> paises) {
        Pais.paises = paises;
    }

    // TO STRING

    @Override
    public String toString() {
        return "Pais{" +
                "nombre='" + nombrePais + '\'' +
                ", codigoGenerado=" + codigoGenerado +
                '}';
    }
}
