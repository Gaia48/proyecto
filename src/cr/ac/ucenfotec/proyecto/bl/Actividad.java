package cr.ac.ucenfotec.proyecto.bl;

import java.util.ArrayList;

public class Actividad {
    private String nombreActividad;
    private int codigoGenerado;
    private String imagen;
    static ArrayList<String> actividades = new ArrayList<>();

    // CONSTRUCTORES

    public Actividad (){
        setNombreActividad("sin nombre de actividad");
        setCodigoGenerado(0);
        setImagen("sin imagen");
    }

    public Actividad(String nombreActividad, int codigoGenerado, String imagen){

        setNombreActividad(nombreActividad);
        setCodigoGenerado(codigoGenerado);
        setImagen(imagen);
    }
    // SETTERS & GETTERS


    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public int getCodigoGenerado() {
        return codigoGenerado;
    }

    public void setCodigoGenerado(int codigoGenerado) {
        this.codigoGenerado = codigoGenerado;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public static ArrayList<String> getActividades() {
        return actividades;
    }

    public static void setActividades(ArrayList<String> actividades) {
        Actividad.actividades = actividades;
    }

    // TO STRING


    @Override
    public String toString() {
        return "Actividad{" +
                "nombreActividad='" + nombreActividad + '\'' +
                ", codigoGenerado=" + codigoGenerado +
                ", imagen='" + imagen + '\'' +
                '}';
    }
}
