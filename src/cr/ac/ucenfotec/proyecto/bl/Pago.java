package cr.ac.ucenfotec.proyecto.bl;

public class Pago {
    private int numero;
    private String provedor;
    private String fechaExp;
    private int codigoTargeta;

    // CONSTRUCTORES

    public Pago(){
        setNumero(0);
        setProvedor("sin provedor");
        setFechaExp("sin fecha de expiracion");
        setCodigoTargeta(0);
    }
    public Pago(int numero, String provedor, String fechaExp, int codigoTargeta){
        setNumero(numero);
        setProvedor(provedor);
        setFechaExp(fechaExp);
        setCodigoTargeta(codigoTargeta);
    }
    // SETTERS & GETTERS


    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getProvedor() {
        return provedor;
    }

    public void setProvedor(String provedor) {
        this.provedor = provedor;
    }

    public String getFechaExp() {
        return fechaExp;
    }

    public void setFechaExp(String fechaExp) {
        this.fechaExp = fechaExp;
    }

    public int getCodigoTargeta() {
        return codigoTargeta;
    }

    public void setCodigoTargeta(int codigoTargeta) {
        this.codigoTargeta = codigoTargeta;
    }

    // TO STRING
}
