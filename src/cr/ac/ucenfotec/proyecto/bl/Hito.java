package cr.ac.ucenfotec.proyecto.bl;

import java.util.ArrayList;

public class Hito extends Reto{

    private int kilometro;
    static ArrayList<String> hitos = new ArrayList<>();

    // CONTRUCTORES

    public Hito (){
        super();
        setKilometro(0);
    }
    public Hito (int kilometro, int codigo, String descripcion, double cantidadKilometros, boolean medalla,
                String nombreActividad, int codigoGenerado, String imagen ){
        super(codigo,descripcion,cantidadKilometros,medalla,nombreActividad, codigoGenerado, imagen);
        setKilometro(kilometro);

    }


    // SETTERS & GETTERS


    public int getKilometro() {
        return kilometro;
    }

    public void setKilometro(int kilometro) {
        this.kilometro = kilometro;
    }

    // TO STRING
}
