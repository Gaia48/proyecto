package cr.ac.ucenfotec.proyecto.bl;

import java.util.ArrayList;

public class Reto extends Actividad{
    private int codigo;
    private String descripcion;
    private double cantidadKilometros;
    private boolean medalla;
    static ArrayList<String> retos = new ArrayList<>();

    // CONSTRUCTORES

    public Reto (){
        super();
        setCodigo(0);
        setDescripcion("sin descripcion");
        setCantidadKilometros(0);
        setMedalla(false);
    }

    public Reto (int codigo, String descripcion, double cantidadKilometros, boolean medalla,
                 String nombreActividad, int codigoGenerado, String imagen){
        super(nombreActividad,codigoGenerado, imagen);
    }
    // SETTERS & GETTERS


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getCantidadKilometros() {
        return cantidadKilometros;
    }

    public void setCantidadKilometros(double cantidadKilometros) {
        this.cantidadKilometros = cantidadKilometros;
    }

    public boolean isMedalla() {
        return medalla;
    }

    public void setMedalla(boolean medalla) {
        this.medalla = medalla;
    }

    public static ArrayList<String> getRetos() {
        return retos;
    }

    public static void setRetos(ArrayList<String> retos) {
        Reto.retos = retos;
    }

    // TO STRING

    @Override
    public String toString() {
        return "Reto{" +
                "codigo=" + codigo +
                ", descripcion='" + descripcion + '\'' +
                ", cantidadKilometros=" + cantidadKilometros +
                ", medalla=" + medalla +
                '}';
    }
}
